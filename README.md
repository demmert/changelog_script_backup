## README

## Changelog & Script Backup Tool

This QSYS User Component helps provide a structured/repeatable way to backup scripts and associated files for version control, backup, or recovery, as well as bundles the changelog functionality and includes copies of the changelog in the backups for easy reference of what was added between each save point. There are 4 space management modes to aid in keeping the files from piling up (keep all, last 10, last 5, last 1), as well as a manual "purge all but latest".

Make sure to set the clock/timezone on the Core prior to saving backups to avoid potential loss due to timestamp comparisons.
The default directory for backups is mdeia/ScriptBackup, but can be edited easily in the script.
10 pins for code input are defined, but more may be easily added by editing the count on the "LUAcode" control of the text controller.
Keeping this as a user component instead of plugin to allow editing for customized backup needs.

Both the user component and a full example file are posted in the download files.

Credit to Kevin Rhodus (QSC) for the change log plugin bundled in the user component.

I'm fairly new to LUA as well as writing "real" programs, and welcome suggestions or comments. If you make changes/additions please reach out so I can learn from you!

I hope this helps improve workflow for others (I know I need the ability to save and reference past versions easily!)